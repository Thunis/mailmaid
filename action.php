<?php

require_once("util.php");

abstract class Action {
    public abstract function on_form_show();
    public abstract function on_form_submit();
    public abstract function on_verification_link();
    protected $actionstr;
    protected function generate_link($params) {
        assert(isset($this->actionstr));
        $addparams = array("action"=>$this->actionstr, "step"=>2);
        $allparams = array_merge($params, $addparams);
        $util = Util::get_instance();
        $getstr = $util->generate_signed_link($allparams);
        $url = $util->get_config("base_url") . '/?' . $getstr;
        return $url;
    }
    public function get_actionstr() {
        return $this->actionstr;
    }
}
