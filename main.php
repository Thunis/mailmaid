<?php

require_once("actions.php");
require_once("util.php");

class Main {
    public function main() {
        // set UTF-8, time zone and locale
        header ("Content-Type: text/html; charset=utf-8");
        date_default_timezone_set("Europe/Berlin"); // TODO make this configurable
        mb_internal_encoding('UTF-8');
        mb_http_output('UTF-8');
        mb_http_input('UTF-8');
        mb_language('uni');
        mb_regex_encoding('UTF-8');
        ob_start('mb_output_handler');
        setlocale(LC_ALL, "de_DE"); // TODO make this configurable
        Util::redirect_to_https();
        $message;
        try {
            $this->header();
            // so error messages will be on top
            if (!ob_start()) {
                // I don't think this can actually happen
                throw new InternalException("call to 'ob_start' failed");
            }
            $this->throwing_main();
        } catch (InternalException $e) {
            Util::get_instance()->log(array("error"=>"InternalException","message"=>$e->getMessage(),"stacktrace"=>$e->getTraceAsString(),"link"=>$_SERVER["QUERY_STRING"]));
            if ($this->send_error_mail($e)) {
                $message = "Es ist ein interner Fehler aufgetreten. Ein Administrator wurde benachrichtigt und wird das Problem schnellstmöglich beheben.";
            } else {
                $message = "Es ist ein schwerwiegender Fehler aufgetreten. Bitte wende dich an <a href='kontakt@thunis-uni.de'>kontakt@thunis-uni.de</a>.";
            }
        } catch (LinkInvalidException $e) {
            Util::get_instance()->log(array("error"=>"LinkInvalidException","link"=>$_SERVER["QUERY_STRING"]));
            $message = "Verifikation fehlgeschlagen!\nVersuch mal, den Link aus der Email rauszukopieren.\nFalls auch das fehlschlägt, wende dich bitte an <a href='kontakt@thunis-uni.de'>kontakt@thunis-uni.de</a>.";
        } catch (LinkTimeoutException $e) {
            // This logging is probably not allowed in EU-DSGVO.
            //Util::get_instance()->log(array("error"=>"LinkTimeoutException","link"=>$_SERVER["QUERY_STRING"]));
            $message = "Der von dir verwendete Verifikationslink ist zu alt. Bitte fordere einen neuen an.";
        } catch (Exception $e) {
            Util::get_instance()->log(array("error"=>"Exception","link"=>$_SERVER["QUERY_STRING"],"message"=>$e->getMessage()));
            $message = $e->getMessage();
        } finally {
            if (isset($message)) {
                // throw the body away and show only the error message
                ob_end_clean();
                echo '<div class="alert alert-danger" role="alert">', $message, '</div>', PHP_EOL;
            } else {
                // flush the body to the user
                ob_end_flush();
            }
            $this->footer();
        }
    }
    private function throwing_main() {
        if (empty($_GET["action"])) {
            $action_str = "lvl";
        } else {
            $action_str = $_GET["action"];
        }
        $action = get_action($action_str);
        assert($action->get_actionstr() == $action_str);
        if (empty($_GET["step"])) {
            $step = 0;
        } else {
            $step = intval($_GET["step"]);
        }
        switch ($step) {
            case 0:
                $action->on_form_show();
                break;
            case 1:
                $action->on_form_submit();
                break;
            case 2:
                Util::get_instance()->verify_link();
                $action->on_verification_link();
                break;
            default:
                throw new LinkInvalidException();
        }
    }
    protected function send_error_mail($e) {
        $mail_text = "Hallo,\nEs gibt ein Problem mit MailMaid in '".$e->getFile()."' in line ".$e->getLine().":\n" . $e->getMessage() . "\nTrace:\n" . $e->getTraceAsString() . "\nViel Spass beim Reparieren!\nMailMaid";
        return mail(Util::get_instance()->get_config('admin_mail'), "MailMaid InternalException", $mail_text);
    }
    
    public function header() {
?>
<!DOCTYPE html>
<html lang="de" class="no-js">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Thunis MailingListen Verwaltung</title>
    <link rel="stylesheet" href="bootstrap.min.css">
    <link rel="stylesheet" href="thunis.css" />
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3 panel panel-default thunisBox">
            <h3 class="panel-heading"><a href="https://lists.thunis-uni.de"><img src="https://wiki.thunis-uni.de/w/images/7/71/Logo_135px.png" alt="" width="32" height="32"> Thunis Mailingliste</a></h3>
            <div class="panel-body">
<?php
    }
    
    public function footer() {
?>
            </div>
            <div class="panel-footer"><a href="https://wiki.thunis-uni.de/wiki/Mailingliste">Was ist diese Seite?</a>&emsp;<a href="https://wiki.thunis-uni.de/wiki/Impressum">Impressum</a></div>
        </div>
    </div>
</div>
</body>
</html>
<?php
    }
}
