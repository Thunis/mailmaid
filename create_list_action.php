<?php

require_once("action.php");
require_once("util.php");

class CreateListAction extends Action {
    public function __construct() {
        $this->actionstr = "cl";
    }
    public function on_form_show() {
        ?><form method="post" enctype="application/x-www-form-urlencoded">
            <div>
                <div>
                    <label for="email">Deine Mailadresse:</label>
                    <input class="form-control" id="email" type="email" name="email" placeholder="E-Mail Adresse" required autofocus>
                </div>
                <div style="display:none">
                    <label for="email">Dieses Feld bitte freilassen:</label>
                    <input id="username" type="text" name="username"><?php /* spam protection: if something is in this field, it's a spam message. */ ?>
                </div>
                <div>
                    <label for="mlname">Name der Mailingliste (nur Kleinbuchstaben und Ziffern erlaubt):</label>
                    <input class="form-control" id="mlname" type="text" name="mlname" placeholder="meintollesprojekt" required>
                </div>
                <div>
                    <label for="members">Mitglieder (eine E-Mail-Adresse pro Zeile):</label>
                    <textarea class="form-control" rows="15" id="members" name="members"></textarea>
                </div>
                
                <div class="buttons">
                    <input id="confirm" type="submit" class="btn btn-success" formaction="index.php?action=cl&step=1" value="Mailingliste erstellen">
                </div>
            </div>
        </form><?php
    }
    public function on_form_submit() {
        // spam protection TODO refactor to main.php
        if (!empty($_POST['username'])) {
            echo "Registration was successful.<script>document.body.innerHTML=atob('V2UgdGhpbmsgeW91IGFyZSBhIHNwYW1ib3QgYmVjYXVzZSB5b3UgZmlsbGVkIG91dCBhIGZvcm0gZmllbGQgdGhhdCBzaG91bGQgYmUgaW52aXNpYmxlIGluIHlvdXIgYnJvd3NlciAocmlnaHQgbmV4dCB0byB0aGUgRS1NYWlsIGZpZWxkKS4=');</script>";
            return;
        }
        // read data from the form
        if (!isset($_POST['email']) || empty($_POST['email'])) throw new Exception('Keine Mailadresse eingegeben!');
        $email = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);
        if (false === $email) throw new Exception('Ungültige Mailadresse!');
        if (!isset($_POST['mlname']) || empty($_POST['email'])) throw new Exception('Kein Name für die Liste angegeben!');
        $mlname = self::validate_mlname($_POST['mlname']);
        if (!isset($_POST['members'])) throw new Exception('Das Mitgliederfeld fehlte in der Übertragung. Versuch\'s mal mit einem anderen Browser.');
        // we just treat this as an opaque string until on_verification_link
        $members = $_POST['members'];
        // we don't validate members E-Mail addresses for several reasons
        // 1. if we say "oh this address is invalid: $address" we create a reflection attack
        // 2. https://hackernoon.com/the-100-correct-way-to-validate-email-addresses-7c4818f24643 (just that in our case the list creator is responsible for checking if everybody gets the E-Mails)
        
    }
    protected static function validate_mlname($s) {
        if (!preg_match('/^[a-z0-9]+$/', $s)) {
            throw new Exception("Ungültiger Mailinglistenname. Erlaubt sind nur Kleinbuchstaben und Ziffern.");
        }
        return $s;
    }
    public function on_verification_link() {
        throw new InternalException("Not implemented yet!");
        // yes, a functional programmer would put the following statements into one expression.
        $members = explode("\n", $_POST['members']);
        $members = array_map("trim", $members);
        $members = array_filter($members, function($s){return strlen($s)>2;});
    }
}
