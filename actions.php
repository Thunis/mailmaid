<?php

require_once("level_action.php");
require_once("create_list_action.php");
require_once("modify_list_action.php");

function get_action($action_str) {
    static $ACTIONS = array(
        "lvl" => "LevelAction",
        "cl"  => "CreateListAction",
        "ml"  => "ModifyListAction",
    );
    if (array_key_exists($action_str, $ACTIONS)) {
        return new $ACTIONS[$action_str]();
    } else {
        throw new LinkInvalidException();
    } 
}
