<?php

class InternalException extends Exception {}
class LinkInvalidException extends Exception {}
class LinkTimeoutException extends Exception {}

class Util {
    protected $config;
    protected static $_instance = null;
    public static function get_instance() {
        if (null === self::$_instance) {
           self::$_instance = new self;
       }
       return self::$_instance;
    }
    protected function __construct() {}
    protected function __clone() {}
    public function get_config($key) {
        if (!isset($this->config)) {
            $jsonstring = file_get_contents("config.json");
            $this->config = json_decode($jsonstring, true);
        }
        if(!is_array($this->config)) {
             throw new InternalException("Irgendwas mit der Config-Datei stimmt nicht.");
        }
        if (!array_key_exists($key, $this->config)) {
            throw new InternalException("In der Config fehlt $key");
        }
        return $this->config[$key];
    }
    public function log($json) {
        $fname = $this->get_config("log_file");
        $date_arr = array("date" => date("c"));
        $merged_arr = array_merge($json, $date_arr);
        $msg = json_encode($merged_arr);
        if ($msg === false) {
            throw new InternalException("unloggable content (not convertible to JSON)");
        }
        file_put_contents($fname, $msg.",\n", FILE_APPEND | LOCK_EX);
    }
    /*
     * To avoid confusion, this always verifies the currently active link.
     * Throws a LinkInvalidException if the mac (or anything else about the link) is invalid.
     * Thwows a LinkTimeoutException if the link is outdated but valid.
     * Returns on success (without a value).
     */
    public function verify_link() {
        // verify mac
        $get = urldecode($_SERVER["QUERY_STRING"]);
        $macstart = strrpos($get, '&mac=');
        if ($macstart === false) {
            throw new LinkInvalidException();
        }
        $verifiable = substr($get, 0, $macstart);
        $calc_mac = $this->mac($verifiable);
        $got_mac = substr($get, $macstart+5 /* strlen('&mac=') */);
        if ($got_mac != $calc_mac) {
            throw new LinkInvalidException();
        }
        // verify time
        if (!isset($_GET["time"])) {
            throw new LinkInvalidException();
        }
        $time = intval($_GET["time"]);
        if (time() > $time + 60*60*24*3) { // TODO make this configurable
            throw new LinkTimeoutException();
        }
    }
    /*
     * Expects an associative array, like http_build_query, and then adds a timestamp ("time")
     * and signature ("mac") parameter.
     */
    public function generate_signed_link($params) {
        $params = array_merge($params, array("time"=>time()));
        $query = http_build_query($params);
        $query_noesc = urldecode($query);
        $mac = $this->mac($query_noesc);
        assert(urlencode($mac) == $mac);
        return $query . '&mac=' . $mac;
    }
    protected function mac($s) {
        $secret = $this->get_config("secret");
        return hash_hmac('sha256', $s, $secret);
    }
    public function is_member($mail) {
        $membercsvfilename = $this->get_config("members_csv");
        $membercsvfile = fopen($membercsvfilename, "r");
        $headrow = fgetcsv($membercsvfile);
        if (!$headrow) {
            throw new InternalException("members_csv doesn't have a valid header");
        }
        $email_column = null;
        foreach ($headrow as $i => $heading) {
            if (strtolower($heading) == "email") {
                $email_column = $i;
                break;
            }
        }
        if ($email_column === null) {
            $this->log(["type"=>"error","message"=>"invalid members_csv header","header"=>$headrow]);
            throw new InternalException("members_csv does not have an email column");
        }
        while (true) {
            $arr = fgetcsv($membercsvfile);
            if ($arr === false) {
                // EOF or other error
                return false;
            }
            if ($arr === null) {
                throw new InternalException("members_csv seems not to point to a valid file");
            }
            if (count($arr) == 1 && $arr[0] === null) {
                // empty line -> skip
                continue;
            }
            if (strtolower($arr[$email_column]) == strtolower($mail)) {
                return true;
            }
        }
    }
    /*
     * Reports the un-subscription of a member to the admin
     */
    public function report_unsubscription($mail, $level) {
        $adminMail = $this->get_config("admin_mail");
        $success = mail($adminMail, 'Thunis Mitglied Listenabmeldung', "Das Thunis-Mitglied mit der Adresse $mail hat sich von Level $level abgemeldet.\n\nMailMaid");
        if (!$success) {
            log(["type"=>"error","message"=>"Could not send E-Mail to the admin about a list unsubscription", "adminMail"=>$adminMail]);
        }
    }
    static public function clean_umlauts($s) {
        $original     = array( "ä",  "ö",  "ü",  "Ä",  "Ö",  "Ü",  "ß");
        $replacements = array("ae", "oe", "ue", "Ae", "Oe", "Ue", "ss");
        return str_replace($original, $replacements, $s);
    }
    static public function level_description($level) {
        switch ($level) {
            case 0:
                return 'Level 0 - Gar keine E-Mails mehr von Thunis erhalten.';
            case 1:
                return 'Level 1 - Nur über Aufführungen informiert werden.';
            case 2:
                return 'Level 2 - Auch die thunisinterne Hauptmailingliste verfolgen.';
            case 3:
                return 'Level 3 - Alle Mailinglisten; auch die Organisationsmailingliste; nur für Mitglieder';
            default:
                throw new InternalException("level_description wanted for unknown level");
        }
    }
    static public function data_implications($level) {
        switch ($level) {
            case 0:
                return "Die angegebene E-Mail-Adresse wird von unserem Server gelöscht.";
            default:
                return "Zur Bereitstellung der Mailinglistenfunktion speichern wir die angegebene Mailadresse auf unserem Server, wo sie nur von den Administratoren eingesehen werden kann. Rechtsgrundlage für die Datenverarbeitung ist Art. 6 EU-DSGVO.";
        }
    }
    static public function redirect_to_https() {
        if (empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off") {
            $redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
            header('HTTP/1.1 301 Moved Permanently');
            header('Location: ' . $redirect);
            exit();
        }
    }
    static public function execute($prog, $args, $cwd, $env) {
        if ($cwd === null)
            $cwd = getenv("HOME");
        if (false === $cwd) throw new InternalException("Die Umgebungsvariable HOME konnte nicht gelesen werden.");

        $escaped_args = array_map("escapeshellarg", $args);
        $cmd = escapeshellarg($prog) . ' ' . implode(' ', $escaped_args);
        $process = proc_open($cmd, [
            0 => ["file", "/dev/zero", "r"],
            1 => ["file", "/dev/null", "a"],
            2 => ["file", "/dev/null", "a"]
        ], $pipes, $cwd, $env);

        if (is_resource($process)) {
            return proc_close($process);
        } else {
            throw new InternalException("Prozess $cmd konnte nicht ausgeführt werden.");
        }
    }
    static public function email_footer() {
        return "Mit freundlichen Grüßen
Thunis e.V.
https://thunis-uni.de";
    }
}
