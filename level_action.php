<?php

require_once("action.php");
require_once("util.php");

class NotThunisMemberException extends Exception {}

class LevelAction extends Action {
    protected static $LISTS = ["announce", "public", "orga"];
    public function __construct() {
        $this->actionstr = "lvl";
    }
    
    public function on_form_show() {
        ?><form method="post" enctype="application/x-www-form-urlencoded">
            <div>
                <div>
                    <label class="sr-only" for="email">Email:</label>
                    <input class="form-control" id="email" type="email" name="email" placeholder="E-Mail Adresse" required autofocus>
                </div>
                <div style="display:none">
                    <label class="sr-only" for="email">Dieses Feld bitte freilassen:</label>
                    <input id="username" type="text" name="username"><?php /* spam protection: if something is in this field, it's a spam message. */ ?>
                </div>
                <?php for ($i=0; $i<4; $i++) { ?>
                <div class="radio">
                   <label><input type="radio" name="levelselect" value="<?=$i?>"  <?=$i==2?"checked":""?>><?=Util::level_description($i)?></label>
                </div>
                <?php } ?>

                <div>
                    <label><input type="checkbox" name="datacheck">&nbsp;Ich bin einverstanden, dass meine E-Mail-Adresse gespeichert wird. <?=Util::data_implications(1)?> Wenn oben "Level 0" ausgewählt ist, werden wir keine Daten speichern und dieses Feld ist gegenstandslos.</label>
                </div>
                <div class="buttons">
                    <input id="confirm" type="submit" class="btn btn-success" formaction="index.php?action=lvl&step=1" value="Bestätigen">
                </div>
            </div>
        </form><?php
    }
    
    public function on_form_submit() {
        // spam protection
        if (!empty($_POST['username'])) {
            echo "Registration was successful.<script>document.body.innerHTML=atob('V2UgdGhpbmsgeW91IGFyZSBhIHNwYW1ib3QgYmVjYXVzZSB5b3UgZmlsbGVkIG91dCBhIGZvcm0gZmllbGQgdGhhdCBzaG91bGQgYmUgaW52aXNpYmxlIGluIHlvdXIgYnJvd3NlciAocmlnaHQgbmV4dCB0byB0aGUgRS1NYWlsIGZpZWxkKS4=');</script>";
            return;
        }
        // read data from the form
        if (!isset($_POST['email']) || empty($_POST['email'])) throw new Exception('Keine Mailadresse eingegeben!');

        $email = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);
        if (FALSE === $email) throw new Exception('Ungültige Mailadresse!');

        if (!isset($_POST['levelselect'])) throw new Exception('Keine Benachrichtigungsstufe gewählt!');
        $level = self::validate_level($_POST['levelselect']);

        if (!isset($_POST['datacheck']) && $level != 0) throw new Exception("Zustimmung zur Datenverarbeitung ist erforderlich für Level $level!");

        // don't check permissions here; check it on activation of the confirmation link
        // This logging is probably not allowed in EU-DSGVO.
        //Util::get_instance()->log(["action"=>"lvl","step"=>1,"email"=>$email,"level"=>$level]);

        // okay, everything read, so, go on
        $link = $this->generate_link(array("email"=>$email,"level"=>$level));
        $body = $this->generate_email_body($link, $level);
        $body = Util::clean_umlauts($body);
        $send_result = mail($email, 'Thunis Mailingliste', $body);
        if (false === $send_result) throw new InternalException("Die Bestätigungsmail an '$email' konnte nicht gesendet werden.");
        echo "Wir haben dir eine Bestätigungsemail geschickt. Sobald du auf den Link in der Mail geklickt hast, 
werden die Änderungen übernommen.";
    }
    protected function generate_email_body($link, $level) {
        return "Hallo,

Du willst deine Benachrichtigungen von Thunis so einstellen:
" . Util::level_description($level) . "

" . Util::data_implications($level) . "

Zur Bestätigung bitte diesen Link anklicken:

$link

Falls diese E-Mail ein Irrtum ist, ist keine weitere Aktion mehr erforderlich.

" . Util::email_footer();
    }
    
    public function on_verification_link() {
        $email = filter_var($_GET['email'], FILTER_VALIDATE_EMAIL);
        if (FALSE === $email) throw new LinkInvalidException();
        $level = self::validate_level($_GET['level']);
        $is_member = Util::get_instance()->is_member($email);
        if ($level == 3 && !$is_member) {
            $level = 2;
            echo '<div class="alert alert-danger" role="alert">Nur Mitglieder von Thunis dürfen die Orga-Mailingliste abonnieren. Du scheinst (noch) kein Mitglied zu sein und bekommst daher ein Abonnement der Stufe 2. Falls du das für einen Fehler hältst, <a href="mailto:kontakt@thunis-uni.de">wende dich bitte an den Ältestenrat</a>.</div>';
        }
        // actually (un-)subscribe for all lists
        for ($i=1; $i<4; $i++) {
            $name = self::$LISTS[$i-1];
            if ($level >= $i) {
                $cmd = "ezmlm-sub";
            } else {
                $cmd = "ezmlm-unsub";
                if ($is_member && $i == 2) {
                    // tell the admins that a member has un-subscribed from this level
                    // currently, this will trigger one email per level, but this should be okay
                    Util::get_instance()->report_unsubscription($email, $i);
                }
            }
            if (0 !== Util::execute($cmd, array("mailinglists/$name", $email), NULL, NULL)) {
                throw new InternalException("Konnte einen Befehl nicht ausführen: $cmd");
            }
        }
        // This logging is probably not allowed in EU-DSGVO.
        //Util::get_instance()->log(["action"=>"lvl","step"=>2,"email"=>$email,"level"=>$level]);
        echo "Glückwunsch! Deine neue E-Mail-Benachrichtigungsstufe ist $level: ".Util::level_description($level);
    }
    
    /**
     * This function checks if the provided email address is allowed on the given mailing list.
     * Throws an exception if no, returns otherwise.
     */
    public function check_permissions($email, $level) {
        if ($level >= 3 && !Util::get_instance()->is_member($email)) throw new NotThunisMemberException();
    }
    static public function validate_level($level_param) {
        $v = intval($level_param);
        if (strval($v) != $level_param || $v < 0 || $v > 3) throw new InternalException('Ungültige Benachrichtigungsstufe: '.strval($level_param));
        return $v;
    }
}
